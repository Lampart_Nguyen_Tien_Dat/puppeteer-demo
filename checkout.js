const puppeteer = require('puppeteer');

(async () => {
	const browser = await puppeteer.launch({
		headless: false, 
		userDataDir: "C:/Users/tien_dat/AppData/Local/Chromium/User Data"
	});
	const page = await browser.newPage();
	
	//ieyasu
 	await page.goto('https://ieyasu.co/lampart1/login/', {waitUntil: 'networkidle2'});
	
	var login = await page.$('.btnSubmit');
	await login.click(); 
	
	await page.goto('https://ieyasu.co/timestamp', {waitUntil: 'networkidle2'});

	var check_out = await page.$("#btnIN2");
	check_out.click();
	await page.evaluate(() => 
		$("#btnIN2").click()
	);
	
	await page.goto('https://ieyasu.co/works', {waitUntil: 'networkidle2'});
	await page.evaluate(() => 
		$(".typeGo").first().click()
	);
	
	await browser.close();
})();