const puppeteer = require('puppeteer');
const download = require('image-downloader');

(async() => {
    const browser = await puppeteer.launch({headless: false});
    const page = await browser.newPage();
    const url = 'http://genk.vn/tim-kiem/hai-huoc.chn';
    const base_url = 'http://genk.vn/';
    await page.goto(url, {waitUntil: 'networkidle2'});

    //Get list websites
    const address_list = await page.evaluate(() => {
        let list = document.querySelectorAll('.list-news-other > li > a');
        list = [...list];
        let address_list = list.map(a => a.getAttribute('href'));
        return address_list;
    });
    
    //Get all img in every website
    for (var i = 0; i < address_list.length; i++) {
        await page.goto(base_url + address_list[i], {waitUntil: 'networkidle2'});
        const imgLinks = await page.evaluate(() => {
            let imgElements = document.querySelectorAll('.VCSortableInPreviewMode.noCaption > div > img');
            imgElements = [...imgElements];
            let imgLinks = imgElements.map(i => i.getAttribute('src'));
            return imgLinks;
        });
        
        await Promise.all(imgLinks.map(imgUrl => download.image({
            url: imgUrl,
            dest: __dirname + "/photos"
        })));

    }

    await browser.close();
})();