const puppeteer = require('puppeteer');
const download = require('image-downloader');

(async() => {
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const url = 'https://news.zing.vn/nhung-noi-ngam-canh-dep-nhat-the-gioi-post667491.html';
    await page.goto(url);

    //Run JS code in evaluate, get src of every image
    const imgLinks = await page.evaluate(() => {
        let imgElements = document.querySelectorAll('.photoset-item > img');
        imgElements = [...imgElements];
        let imgLinks = imgElements.map(i => i.getAttribute('src'));
        return imgLinks;
    });

    //Download all image 
    await Promise.all(imgLinks.map(imgUrl => download.image({
        url: imgUrl,
        dest: __dirname + "/photos"
    })));

    await browser.close();
})();