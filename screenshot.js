const puppeteer = require('puppeteer');

(async () => {
  //Init browser and page
  const browser = await puppeteer.launch({ headless: false });
  const page = await browser.newPage();

  //Set page view port
  page.setViewport({ width: 1280, height:720 });

  // Go to website and wait for load complete
  await page.goto('https://medium.com/', { waitUntil: 'networkidle2' });

  //Take a screenshot and save with specific name
  await page.screenshot({path: 'medium.png'});

  //Close browser
  await browser.close();
})();