let puppeteer = require('puppeteer');
let browser = null;
let page = null;

describe('Landing test', () => {

    beforeAll(async() => {
        browser = await puppeteer.launch();
        page = await browser.newPage();
        await page.setViewport({
            width: 1280,
            height: 720
        });

        jest.setTimeout(20000);
    });

    afterAll(async() => {
        await browser.close();
    });

    //run before each test suite
    beforeEach(async() => {
        await page.goto('https://dev-pw-jp.fraise.jp/landing/server/code/lp100_registration', {waitUntil: 'networkidle2'});
    });

    //first test suite
    test('Search server Tokyo', async() => {
        expect.assertions(1);

        //get element zipcode and input
        const zipcode = await page.$('#txt_confirm_zipcode');
        const submit = await page.$('.check-zipcode');
        await zipcode.type('1500001');
        await submit.click();
        
        //get server and count
        const server = await page.$$('#select-server > ul > li');
        expect(server.length).toBe(5);
        
    });
    
    //second test suite
    test('Search server Okinawa', async() => {
        expect.assertions(1);

        const zipcode = await page.$('#txt_confirm_zipcode');
        const submit = await page.$('.check-zipcode');
        await zipcode.type('9008556');
        await submit.click();
        
        const server = await page.$$('.okinawa_in > ul > li');
        expect(server.length).toBe(5);
    });

})

